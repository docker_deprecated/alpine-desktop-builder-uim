# alpine-desktop-builder-uim

#### [alpine-x64-desktop-builder-uim](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-desktop-builder-uim/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpinex64build/alpine-x64-desktop-builder-uim/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpinex64build/alpine-x64-desktop-builder-uim/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpinex64build/alpine-x64-desktop-builder-uim/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64build/alpine-x64-desktop-builder-uim)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64build/alpine-x64-desktop-builder-uim)
#### [alpine-aarch64-desktop-builder-uim](https://hub.docker.com/r/forumi0721alpineaarch64build/alpine-aarch64-desktop-builder-uim/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpineaarch64build/alpine-aarch64-desktop-builder-uim/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpineaarch64build/alpine-aarch64-desktop-builder-uim/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpineaarch64build/alpine-aarch64-desktop-builder-uim/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpineaarch64build/alpine-aarch64-desktop-builder-uim)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpineaarch64build/alpine-aarch64-desktop-builder-uim)
#### [alpine-armhf-desktop-builder-uim](https://hub.docker.com/r/forumi0721alpinearmhfbuild/alpine-armhf-desktop-builder-uim/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpinearmhfbuild/alpine-armhf-desktop-builder-uim/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpinearmhfbuild/alpine-armhf-desktop-builder-uim/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpinearmhfbuild/alpine-armhf-desktop-builder-uim/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinearmhfbuild/alpine-armhf-desktop-builder-uim)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinearmhfbuild/alpine-armhf-desktop-builder-uim)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64,armhf
* Appplication : [UIM](https://github.com/uim/uim/)  (Build Image)
    - uim (short for "universal input method") is a multilingual input method framework. Applications can use it through so-called bridges.



----------------------------------------
#### Run

* Nothing



----------------------------------------
#### Usage

* Nothing



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

